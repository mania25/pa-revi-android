package com.revi.curtaincontrolsystem.Interface;

import com.revi.curtaincontrolsystem.Models.UserRegistration;
import com.revi.curtaincontrolsystem.Models.UserToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Abdurrahman on 28-May-17.
 */

public interface UserLoginAPI {
    @FormUrlEncoded
    @POST("/api/login")
    Call<UserToken> userLogin (
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("/api/register")
    Call<UserRegistration> userRegistration(
            @Field("username") String username,
            @Field("password") String password
    );
}