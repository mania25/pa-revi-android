package com.revi.curtaincontrolsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.revi.curtaincontrolsystem.Interface.UserLoginAPI;
import com.revi.curtaincontrolsystem.Models.UserToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView lblRegister;

    private SharedPreferences userSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText)findViewById(R.id.txtUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        lblRegister = (TextView)findViewById(R.id.lblRegister);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://137.59.126.83:5050")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final UserLoginAPI userLoginAPI = retrofit.create(UserLoginAPI.class);
        userSharedPreferences = getSharedPreferences("ProyekAkhir", MODE_PRIVATE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtUsername.getText().toString()) || TextUtils.isEmpty(txtPassword.getText().toString())){
                    Toast.makeText(LoginActivity.this, "Username or Password Cannot Be Empty", Toast.LENGTH_SHORT).show();
                } else {
                    Call<UserToken> userTokenCall = userLoginAPI.userLogin(txtUsername.getText().toString(), txtPassword.getText().toString());
                    userTokenCall.enqueue(new Callback<UserToken>() {
                        @Override
                        public void onResponse(Call<UserToken> call, Response<UserToken> response) {
                            if (response.code() == 200){
                                String token = response.body().getToken();
                                String data = response.body().getData();

                                SharedPreferences.Editor editor  = userSharedPreferences.edit();
                                editor.putString("AT", token);
                                editor.putString("userProfile", data);
                                editor.commit();

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(LoginActivity.this, "Username or Password Invalid", Toast.LENGTH_LONG);
                            }
                        }

                        @Override
                        public void onFailure(Call<UserToken> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, "Username or Password Invalid", Toast.LENGTH_LONG);
                        }
                    });
                }
            }
        });

        lblRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }
}
