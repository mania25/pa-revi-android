package com.revi.curtaincontrolsystem;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioAutomaticMode, radioManualMode, radioIntervalMode;
    private TextView manualState;
    private CardView manualCVLayout;
    private LinearLayout layoutInputInterval;
    private EditText txtInterval;
    private Button btnSubmit;

    private final String mqttBrokerUrl = "tcp://137.59.126.83:1883";
    private MqttAndroidClient mqttAndroidClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioAutomaticMode = (RadioGroup) findViewById(R.id.radioAutomaticMode);
        radioManualMode = (RadioGroup) findViewById(R.id.radioManualMode);
        manualState = (TextView)findViewById(R.id.manualState);
        manualCVLayout = (CardView)findViewById(R.id.manualCVLayout);
        layoutInputInterval = (LinearLayout)findViewById(R.id.layoutInputInterval);
        radioIntervalMode = (RadioGroup) findViewById(R.id.radioIntervalMode);
        txtInterval = (EditText)findViewById(R.id.txtInterval);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), mqttBrokerUrl, "device-pa-android");
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                if (reconnect){
                    Log.d("Reconnected to:", serverURI);
                } else {
                    Log.d("Connected To:", serverURI);
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.d("Connected Lost:", cause.getMessage());
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d("messageArrived: ", new String(message.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();

        mqttConnectOptions.setUserName("bbff39d0d3066758ffe55666762b3c8b150295b848cb6c871b79f2fff36c79fb");
        mqttConnectOptions.setPassword("50acea3098359517297e08040dc6bfc371d044190be6527c1ac29e078cbe8313".toCharArray());
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic("/controlling");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    exception.printStackTrace();
                    Log.d("Failed to connect to: ", mqttBrokerUrl);
                }
            });
        } catch (Exception ex){
            ex.printStackTrace();
        }

        radioAutomaticMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.radioOff){
                    Toast.makeText(MainActivity.this, "Mode Otomatis Dimatikan", Toast.LENGTH_SHORT).show();
                    manualCVLayout.setVisibility(View.VISIBLE);
                    publishMessage("/controlling", "MANUAL:0");
                } else {
                    Toast.makeText(MainActivity.this, "Mode Otomatis Dinyalakan", Toast.LENGTH_SHORT).show();
                    manualCVLayout.setVisibility(View.GONE);
                    publishMessage("/controlling", "AUTOMATIC");
                }
            }
        });

        radioManualMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.radioClosed){
                    manualState.setText("Manual (Closed)");
                    publishMessage("/controlling", "MANUAL:0");
                } else {
                    manualState.setText("Manual (Open)");
                    publishMessage("/controlling", "MANUAL:1");
                }
            }
        });

        radioIntervalMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.radioIntervalOn){
                    layoutInputInterval.setVisibility(View.VISIBLE);
                } else {
                    layoutInputInterval.setVisibility(View.GONE);
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishMessage("/controlling", "TIMES:" + txtInterval.getText().toString());
            }
        });
    }

    public void subscribeToTopic(String mqttTopic){
        try {
            mqttAndroidClient.subscribe(mqttTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d("onSuccess: ", "SUBSCRIBED");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("Failed to subscribe", exception.getMessage());
                }
            });

            // THIS DOES NOT WORK!
            mqttAndroidClient.subscribe(mqttTopic, 0, new IMqttMessageListener() {
                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    // message Arrived!
                    System.out.println("Message: " + topic + " : " + new String(message.getPayload()));
                }
            });

        } catch (Exception ex){
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void publishMessage(String mqttTopic, String command){

        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(command.getBytes());
            mqttAndroidClient.publish(mqttTopic, message);
            Log.d("publishMessage: ", "Message Published");
            if(!mqttAndroidClient.isConnected()){
                Log.d("publishMessage: ", mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.");;
            }
        } catch (Exception e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
