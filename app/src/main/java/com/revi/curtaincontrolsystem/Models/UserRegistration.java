package com.revi.curtaincontrolsystem.Models;

/**
 * Created by Abdurrahman on 28-May-17.
 */

public class UserRegistration {
    private int id;
    private String username;

    public UserRegistration(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
