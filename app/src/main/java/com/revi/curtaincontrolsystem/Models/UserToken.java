package com.revi.curtaincontrolsystem.Models;

/**
 * Created by Abdurrahman on 28-May-17.
 */

public class UserToken {
    private int code;
    private String token;
    private String data;

    public UserToken(int code, String token, String data) {
        this.code = code;
        this.token = token;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getToken() {
        return token;
    }

    public String getData() {
        return data;
    }
}
