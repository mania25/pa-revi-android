package com.revi.curtaincontrolsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.revi.curtaincontrolsystem.Interface.UserLoginAPI;
import com.revi.curtaincontrolsystem.Models.UserRegistration;
import com.revi.curtaincontrolsystem.Models.UserToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends AppCompatActivity {
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        txtUsername = (EditText)findViewById(R.id.txtUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        btnRegister = (Button)findViewById(R.id.btnRegister);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://137.59.126.83:5050")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final UserLoginAPI userLoginAPI = retrofit.create(UserLoginAPI.class);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtUsername.getText().toString()) || TextUtils.isEmpty(txtPassword.getText().toString())){
                    Toast.makeText(RegistrationActivity.this, "Username or Password Cannot Be Empty", Toast.LENGTH_SHORT).show();
                } else {
                    Call<UserRegistration> userRegistrationCall = userLoginAPI.userRegistration(txtUsername.getText().toString(), txtPassword.getText().toString());
                    userRegistrationCall.enqueue(new Callback<UserRegistration>() {
                        @Override
                        public void onResponse(Call<UserRegistration> call, Response<UserRegistration> response) {
                            if (response.code() == 200){
                                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(RegistrationActivity.this, "Registration Success", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegistrationActivity.this, "Registration Failed", Toast.LENGTH_LONG);
                            }
                        }

                        @Override
                        public void onFailure(Call<UserRegistration> call, Throwable t) {
                            Toast.makeText(RegistrationActivity.this, "Registration Failed", Toast.LENGTH_LONG);
                        }
                    });
                }
            }
        });
    }
}
